package com.bol.assignment;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;

@SuppressWarnings({"squid:S00116"})
public class GameLogicTest
{
	private GameLogic _instance;
	private final MancalaUI _ui = new MancalaUI();

	@Mock
	private VaadinRequest _vaadinRequest;

	@Before
	public void setup()
	{
		_ui.init(_vaadinRequest);
		_instance = _ui._gameLogic;
	}

	@Test
	public void testPitClick()
	{
		// Default beginner is Player1
		assertThat(_instance.getCurrentPlayer()).isEqualTo(Player.PLAYER1);

		// Player1 clicks on the fourth pit
		((Button)_instance.getPitsPlayer1().get(3)).click();

		// Player2 gets the turn
		assertThat(_instance.getCurrentPlayer()).isEqualTo(Player.PLAYER2);
		assertPitEnablement(_instance.getPitsPlayer1(), false);
		assertPitEnablement(_instance.getPitsPlayer2(), true);
	}

	@Test
	public void testSetPlayerTurn()
	{
		final Player currentPlayer = _instance.getCurrentPlayer();

		_instance.setPlayerTurn();

		assertThat(_instance.getCurrentPlayer()).isEqualTo(currentPlayer);

		assertThat(_instance.getStatusPlayer1().getValue()).contains(currentPlayer.toString());
		assertThat(_instance.getStatusPlayer2().getValue()).contains(currentPlayer.toString());

		assertThat(_instance.getPitsPlayer1()).isNotEmpty();
		assertThat(_instance.getPitsPlayer2()).isNotEmpty();

		// Player1 should be the current player
		assertPitEnablement(_instance.getPitsPlayer1(), true);
		assertPitEnablement(_instance.getPitsPlayer2(), false);
	}

	@Test
	public void testExecuteGameLogic()
	{
		// Simulate a situation where the player 1 clicks on the first pit
		_instance.executeGameLogic(_instance.getPitsPlayer1().get(0));

		assertThat(_instance.getPitsPlayer1().get(0).getCaption()).isEqualTo("0");
		for (int i = 1; i < _instance.getPitsPlayer1().size(); i++)
		{
			assertPitContent(_instance.getPitsPlayer1().get(i), "7");
		}
		assertThat(((TextArea)_instance.getAllPits().get(Constants.PLAYER_1_BIG_PIT_INDEX)).getValue()).contains("1");

		for (Component pit : _instance.getPitsPlayer2())
		{
			assertPitContent(pit, "6");
		}
		assertThat(((TextArea)_instance.getAllPits().get(Constants.PLAYER_2_BIG_PIT_INDEX)).getValue()).contains("0");

		// Turn ends on a big pit, player 1 gets another turn
		assertThat(_instance.getCurrentPlayer()).isEqualTo(Player.PLAYER1);
	}

	@Test
	public void testStoneCapture()
	{
		/* Simulate this:
			1. Player 1 clicks on the first pit, turn ends on a big pit, Player 1 gets another turn
			2. Player 1 clicks on the second pit, Player 2 gets the turn
			3. Player 2 clicks on the first pit, Player 1 gets the turn,
					Player 1's second pit is empty, first pit has 1 stone and big pit has 2 stones,
					Player 2's fifth pit (corresponding pit of Player 1's second pit) has 7 stones.
			4. Player 1 clicks on the first pit and captures Player 2's fifth pit,
					Player 1's first and second pits become empty, big pit has 10 stones, Player 2's fifth pit becomes empty.
		*/

		/*1*/
		_instance.executeGameLogic(_instance.getPitsPlayer1().get(0));
		/*2*/
		_instance.executeGameLogic(_instance.getPitsPlayer1().get(1));
		/*3*/
		_instance.executeGameLogic(_instance.getPitsPlayer2().get(0));
		/*4*/
		_instance.executeGameLogic(_instance.getPitsPlayer1().get(0));

		assertThat(_instance.getPitsPlayer1().get(0).getCaption()).isEqualTo("0");
		assertThat(_instance.getPitsPlayer1().get(1).getCaption()).isEqualTo("0");
		assertThat(_instance.getPitsPlayer2().get(4).getCaption()).isEqualTo("0");
		assertThat(((TextArea)_instance.getAllPits().get(Constants.PLAYER_1_BIG_PIT_INDEX)).getValue()).contains("10");
	}

	private void assertPitContent(final Component pit, final String expectedNumberOfStones)
	{
		assertThat(pit.getCaption()).as("The pit has different number of stones than expected.").isEqualTo(expectedNumberOfStones);
	}

	private void assertPitEnablement(final List<Component> pits, final boolean isEnabled)
	{
		for (Component pit : pits)
		{
			if (isEnabled)
			{
				assertThat(pit.isEnabled()).isTrue();
			}
			else
			{
				assertThat(pit.isEnabled()).isFalse();
			}
		}
	}
}
