package com.bol.assignment;

public class Constants
{
	private Constants() {}

	public static final String PIT = "Pit";
	public static final String BIG_PIT_PLAYER = "BigPitPlayer";
	public static final String CURRENT_PLAYER_IS_PLAYER = "Current Player is ";

	public static final int PLAYER_1_BIG_PIT_INDEX = 6;
	public static final int PLAYER_2_BIG_PIT_INDEX = 13;
	public static final int NUMBER_OF_SMALL_PITS = 6;
	public static final int INITIAL_NUMBER_OF_STONES = 6;
	public static final String TEXT_AREA_VALUE_PREFIX = "\n\n";
}
