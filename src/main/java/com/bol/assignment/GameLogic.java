package com.bol.assignment;

import static com.bol.assignment.Constants.CURRENT_PLAYER_IS_PLAYER;
import static com.bol.assignment.Constants.PLAYER_1_BIG_PIT_INDEX;
import static com.bol.assignment.Constants.PLAYER_2_BIG_PIT_INDEX;
import static com.bol.assignment.Player.PLAYER1;
import static com.bol.assignment.Player.PLAYER2;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import de.steinwedel.messagebox.MessageBox;

@SuppressWarnings({"squid:S00116"})
public class GameLogic implements Serializable
{
	private static final long serialVersionUID = 8331726931066831474L;

	// Player 1 always starts the game, therefore the default is PLAYER1.
	private Player _currentPlayer = PLAYER1;

	// Turn indicator - shows who the current player is.
	// A label per player guarantees that the top and bottom layouts are aligned.
	private final Label _statusPlayer1 = new Label(CURRENT_PLAYER_IS_PLAYER + _currentPlayer);
	private final Label _statusPlayer2 = new Label(CURRENT_PLAYER_IS_PLAYER + _currentPlayer);

	// Player button collections. This comes in handy when iterating over and making changes on them.
	private final List<Component> _pitsPlayer1 = new ArrayList<>();
	private final List<Component> _pitsPlayer2 = new ArrayList<>();

	// Collection of all pits - to be used in game logic.
	private final List<Component> _allPits = new ArrayList<>();

	protected void setPlayerTurn()
	{
		// Set the status labels.
		_statusPlayer1.setValue(CURRENT_PLAYER_IS_PLAYER + _currentPlayer);
		_statusPlayer2.setValue(CURRENT_PLAYER_IS_PLAYER + _currentPlayer);

		// Enable current player's pits and disable the other's pits.
		togglePits(_pitsPlayer1, _currentPlayer == PLAYER1);
		togglePits(_pitsPlayer2, _currentPlayer == PLAYER2);
	}

	protected void togglePits(final List<Component> pits, final boolean isEnable)
	{
		for (Component pit : pits)
		{
			pit.setEnabled(isEnable);
		}
	}

	protected void emptyPit(final Component pit)
	{
		pit.setCaption("0");
	}

	protected void executeGameLogic(final Component clickedPit)
	{
		final int clickedPitIndex = _allPits.indexOf(clickedPit);

		// The number of stones that are picked at a turn.
		int numberOfPickedStones = Integer.parseInt(clickedPit.getCaption());
		// Begin distributing stones with the next pit.
		final int iterationBeginIndex = clickedPitIndex + 1;

		final int currentPlayerBigPitIndex = _currentPlayer == PLAYER1 ? PLAYER_1_BIG_PIT_INDEX : PLAYER_2_BIG_PIT_INDEX;

		// Hold the reference to the last updated component in order to later determine if the turn ends
		// in a big pit, small pit or an empty pit.
		Component lastUpdatedComponent = null;

		for (int rawIndex = 0; numberOfPickedStones != 0; rawIndex++, numberOfPickedStones--)
		{
			int indexToUse = rawIndex + iterationBeginIndex;
			// Ensure the round distribution of stones.
			if (indexToUse >= _allPits.size())
			{
				indexToUse = indexToUse % _allPits.size();
			}

			// Select the pit.
			final Component pit = _allPits.get(indexToUse);

			final int stonesInPit;
			if (pit instanceof Button)
			{
				// Add a stone to a small pit.
				stonesInPit = Integer.parseInt(pit.getCaption());
				pit.setCaption(String.valueOf(stonesInPit + 1));

				lastUpdatedComponent = pit;
			}
			else if (pit instanceof TextArea && indexToUse == currentPlayerBigPitIndex)
			{
				// Add a stone to a big pit.
				final TextArea bigPit = (TextArea)pit;
				stonesInPit = Integer.parseInt(bigPit.getValue().trim());
				bigPit.setValue(Constants.TEXT_AREA_VALUE_PREFIX + (stonesInPit + 1));

				lastUpdatedComponent = pit;
			}
		}

		// Check whether the turn ended in a small pit.
		if (lastUpdatedComponent != null && lastUpdatedComponent instanceof Button)
		{
			captureStonesIfPitWasEmpty(lastUpdatedComponent, currentPlayerBigPitIndex);

			// The turn ended in a small pit, switch the current player.
			_currentPlayer = getOtherPlayer();
		}
		// Reset the stone content of the pit.
		emptyPit(clickedPit);
	}

	protected void captureStonesIfPitWasEmpty(
		final Component lastUpdatedComponent, final int currentPlayerBigPitIndex)
	{
		final String lastUpdatedComponentId = lastUpdatedComponent.getId();
		final String currentPlayer = _currentPlayer.toString();

		// If the turn ends in an empty pit of the current player, capture the stones in the opposite pit if there is any.
		if (lastUpdatedComponent.getCaption().equals("1") && lastUpdatedComponentId.contains(currentPlayer))
		{
			// Construct an ID that will match the opposite pit's ID.
			final String oppositePitID = lastUpdatedComponentId.replace(currentPlayer, getOtherPlayer().toString());

			// Find the opposite pit in _allPits collection.
			//  (No need to call the isPresent() on the Optional that is returned by findFirst(),
			//  as we are sure that there is always a component with the constructed ID).
			final Component oppositePit =
				_allPits.stream().filter(component -> component.getId().equals(oppositePitID)).findFirst().get(); //NOSONAR

			// Get the number of stones in the opposite pit.
			final int stonesInOppositePit = Integer.parseInt(oppositePit.getCaption());

			if (stonesInOppositePit > 0)
			{
				// Add these stones to the current player's big pit.
				final TextArea bigPit = (TextArea)_allPits.get(currentPlayerBigPitIndex);
				// Calculate the total stones to be put into the big pit:
				//      already present stones + stones in the opposite pit + the last stone.
				final Integer totalStonesInBigPit = Integer.valueOf(bigPit.getValue().trim()) + stonesInOppositePit + 1;
				bigPit.setValue(Constants.TEXT_AREA_VALUE_PREFIX + totalStonesInBigPit.toString());

				// Empty the current pit and the opposite pit.
				emptyPit(oppositePit);
				emptyPit(lastUpdatedComponent);
			}
		}
	}

	protected void checkIfGameIsOver()
	{
		final boolean isPlayer1Finished = isPlayerFinished(_pitsPlayer1);
		final boolean isPlayer2Finished = isPlayerFinished(_pitsPlayer2);

		if (isPlayer1Finished || isPlayer2Finished)
		{
			final String winner = getWinner();
			if (winner.contains("1"))
			{
				_statusPlayer1.setValue("WON!");
				_statusPlayer2.setValue("LOST!");
			}
			else if (winner.contains("2"))
			{
				_statusPlayer1.setValue("LOST!");
				_statusPlayer2.setValue("WON!");
			}
			else
			{
				_statusPlayer1.setValue("DRAW!");
				_statusPlayer2.setValue("DRAW!");
			}

			_statusPlayer1.setStyleName("red");
			_statusPlayer2.setStyleName("red");

			togglePits(_pitsPlayer1, false);
			togglePits(_pitsPlayer2, false);

			//@Formatter:off
			MessageBox
				.createQuestion()
				.withCaption("Game Over! " + winner + "! New game?")
				.withYesButton(() -> Page.getCurrent().reload())
				.withNoButton()
				.withWidth("360")
				.open();
			//@Formatter:on
		}
	}

	private boolean isPlayerFinished(final List<Component> playerPits)
	{
		for (Component component : playerPits)
		{
			if (Integer.parseInt(component.getCaption()) == 0)
			{
				continue;
			}
			return false;
		}
		return true;
	}

	private String getWinner()
	{
		final Integer player1TotalStones = Integer.valueOf(((TextArea)_allPits.get(PLAYER_1_BIG_PIT_INDEX)).getValue().trim());
		final Integer player2TotalStones = Integer.valueOf(((TextArea)_allPits.get(PLAYER_2_BIG_PIT_INDEX)).getValue().trim());
		final int winner = player1TotalStones.compareTo(player2TotalStones);

		if (winner > 0)
		{
			return "Winner is " + PLAYER1.toString();
		}
		else if (winner == 0)
		{
			return "There is no winner";
		}
		else
		{
			return "Winner is " + PLAYER2.toString();
		}
	}

	protected Player getOtherPlayer()
	{
		return _currentPlayer == PLAYER1 ? PLAYER2 : PLAYER1;
	}

	protected Label getStatusPlayer1()
	{
		return _statusPlayer1;
	}

	protected Label getStatusPlayer2()
	{
		return _statusPlayer2;
	}

	protected List<Component> getPitsPlayer1()
	{
		return _pitsPlayer1;
	}

	protected List<Component> getPitsPlayer2()
	{
		return _pitsPlayer2;
	}

	protected void addPitPlayer1(final Component pit)
	{
		_pitsPlayer1.add(pit);
	}

	protected void addPitPlayer2(final Component pit)
	{
		_pitsPlayer2.add(pit);
	}

	protected void addToAllPits(final List<Component> pits)
	{
		_allPits.addAll(pits);
	}

	protected Player getCurrentPlayer()
	{
		return _currentPlayer;
	}

	protected List<Component> getAllPits()
	{
		return _allPits;
	}
}
