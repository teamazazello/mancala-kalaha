package com.bol.assignment;

import static com.bol.assignment.Constants.BIG_PIT_PLAYER;
import static com.bol.assignment.Constants.INITIAL_NUMBER_OF_STONES;
import static com.bol.assignment.Constants.NUMBER_OF_SMALL_PITS;
import static com.bol.assignment.Constants.PIT;
import static com.bol.assignment.Constants.TEXT_AREA_VALUE_PREFIX;
import static com.bol.assignment.Player.PLAYER1;
import static com.bol.assignment.Player.PLAYER2;
import java.util.*;
import javax.servlet.annotation.WebServlet;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;

/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@SuppressWarnings({"squid:S00116", "squid:S2160"})
@Theme("mytheme")
public class MancalaUI extends UI
{
	protected GameLogic _gameLogic = new GameLogic();

	@Override
	protected void init(VaadinRequest vaadinRequest)
	{
		// Top frame - Player 1
		HorizontalLayout horizontalLayoutPlayer1 = prepareHorizontalLayout(PLAYER1);
		final TextArea bigPitPlayer1 = addBigPit(horizontalLayoutPlayer1, PLAYER1);

		// Add the player indicator label on top of the player area.
		horizontalLayoutPlayer1.addComponent(_gameLogic.getStatusPlayer1(), 0);
		horizontalLayoutPlayer1.setComponentAlignment(_gameLogic.getStatusPlayer1(), Alignment.TOP_CENTER);

		/*At this point, elements for Player1 area are:
			0: Status label
			1: Big pit
			2-7: Small pits
			8: PLAYER 1 label
		 */

		// Bottom frame - Player 2
		HorizontalLayout horizontalLayoutPlayer2 = prepareHorizontalLayout(PLAYER2);
		final TextArea bigPitPlayer2 = addBigPit(horizontalLayoutPlayer2, PLAYER2);

		// Add the player indicator label on top of the player area.
		horizontalLayoutPlayer2.addComponent(_gameLogic.getStatusPlayer2(), 0);
		horizontalLayoutPlayer2.setComponentAlignment(_gameLogic.getStatusPlayer2(), Alignment.TOP_CENTER);

		/*At this point, elements for Player2 area are:
		    0: Status label
		    1-6: Small pits
		    7: Big pit
		    8: PLAYER 2 label
		 */

		// Reverse the order of pits in _pitsPlayer1 collection to obtain a list of all pits,
		// starting from the right most pit of Player1.
		Collections.reverse(_gameLogic.getPitsPlayer1());

		// Collect all pits (including the big ones) into one collection.
		// Add Player1's small pits.
		_gameLogic.addToAllPits(_gameLogic.getPitsPlayer1());
		// Add Player1's big pit.
		_gameLogic.addToAllPits(Collections.singletonList(bigPitPlayer1));
		// Add Player2's small pits.
		_gameLogic.addToAllPits(_gameLogic.getPitsPlayer2());
		// Add Player2's big pit.
		_gameLogic.addToAllPits(Collections.singletonList(bigPitPlayer2));

		/*In _allPits collection:
			0-5: Small pits of Player1
			6: Big pit of Player1
			7-12: Small pits of Player2
			13: Big pit of Player2
		*/

		// Create the page split vertically.
		final VerticalSplitPanel verticalSplitPanel = new VerticalSplitPanel(horizontalLayoutPlayer1, horizontalLayoutPlayer2);

		// Set the initial status labels and enable/disable pits.
		_gameLogic.setPlayerTurn();

		// Set page content.
		setContent(verticalSplitPanel);
	}

	protected TextArea addBigPit(final HorizontalLayout horizontalLayout, final Player player)
	{
		// Add a big pit for the player as a TextArea component to the layout.
		final TextArea bigPit = new TextArea();
		bigPit.setId(BIG_PIT_PLAYER + player);
		bigPit.setReadOnly(true);
		bigPit.setValue(TEXT_AREA_VALUE_PREFIX + "0");

		if (player == PLAYER1)
		{
			// For Player1 add the big pit to the beginning of the component collection.
			horizontalLayout.addComponentAsFirst(bigPit);
		}
		else
		{
			// For Player2 add the big pit to the end of the component collection.
			horizontalLayout.addComponent(bigPit);
		}
		horizontalLayout.setComponentAlignment(bigPit, Alignment.MIDDLE_CENTER);


		return bigPit;
	}

	private HorizontalLayout prepareHorizontalLayout(final Player player)
	{
		final HorizontalLayout horizontalLayout = new HorizontalLayout();
		// Looks better than the default height setting.
		horizontalLayout.setHeight(350, Unit.PIXELS);

		// Create and add the pits as button components to the layout & to the player pit collection.
		Button pit;
		for (int i = 1; i <= NUMBER_OF_SMALL_PITS; i++)
		{
			pit = new Button(String.valueOf(INITIAL_NUMBER_OF_STONES), this::click);
			// Set an ID to the pit.
			// This will come in handy when executing the capturing mechanism
			// when the turn ends in an empty pit of the player.
			pit.setId(PIT + i + player);

			horizontalLayout.addComponent(pit);
			horizontalLayout.setComponentAlignment(pit, Alignment.MIDDLE_CENTER);

			if (player == PLAYER1)
			{
				_gameLogic.addPitPlayer1(pit);
			}
			else
			{
				_gameLogic.addPitPlayer2(pit);
			}
		}

		final Label playerLabel = new Label(player.toString());
		horizontalLayout.addComponent(playerLabel);
		horizontalLayout.setComponentAlignment(playerLabel, Alignment.BOTTOM_CENTER);

		return horizontalLayout;
	}

	// This method governs the set of actions that happen when a pit is clicked.
	private void click(Button.ClickEvent event)
	{
		final Button pit = event.getButton();

		_gameLogic.executeGameLogic(pit);

		_gameLogic.setPlayerTurn();

		_gameLogic.checkIfGameIsOver();
	}

	@WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = MancalaUI.class, productionMode = true)
	public static class MyUIServlet extends VaadinServlet
	{
		private static final long serialVersionUID = 8331726900066800474L;
	}
}
